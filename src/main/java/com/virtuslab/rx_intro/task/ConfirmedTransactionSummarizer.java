package com.virtuslab.rx_intro.task;


import io.reactivex.Observable;
import io.reactivex.Single;

import java.math.BigDecimal;
import java.util.function.Supplier;

/**
 * ConfirmedTransactionSummarizer is responsible for calculation of total confirmed transactions value.
 * HINT:
 * - Use zip operator to match transactions with confirmations. They will appear in order
 * - Filter only confirmed
 * - Aggregate value of confirmed transactions using reduce operator
 * <p>
 * HINT2:
 * - add error handling which will wrap an error into SummarizationException
 */
class ConfirmedTransactionSummarizer {

    private final Supplier<Observable<Transaction>> transactions;
    private final Supplier<Observable<Confirmation>> confirmations;

    ConfirmedTransactionSummarizer(Supplier<Observable<Transaction>> transactions,
                                   Supplier<Observable<Confirmation>> confirmations) {
        this.transactions = transactions;
        this.confirmations = confirmations;
    }

    Single<BigDecimal> summarizeConfirmedTransactions() {
        Single<BigDecimal> result;
        result = Single.just(Observable
                .zip(transactions.get(), confirmations.get(), (transaction, confirmation) -> new ConfirmedTransaction(transaction, confirmation))
//                    .doOnError(throwable -> {throw new SummarizationException("Booom");})
                .filter(confirmedTransaction -> confirmedTransaction.getConfirmation().isConfirmed)
                .map(confirmedTransaction -> confirmedTransaction.getTransaction().value)
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .as(l -> (l.blockingGet())));
        return result.doOnError(throwable -> {throw new SummarizationException("Booom");});
    }

    static class SummarizationException extends RuntimeException {

        public SummarizationException(String message) {
            super(message);
        }
    }

    private class ConfirmedTransaction {
        private Transaction transaction;
        private Confirmation confirmation;

        public ConfirmedTransaction(Transaction transaction, Confirmation confirmation) {
            this.transaction = transaction;
            this.confirmation = confirmation;
        }

        public Transaction getTransaction() {
            return transaction;
        }

        public Confirmation getConfirmation() {
            return confirmation;
        }
    }
}
